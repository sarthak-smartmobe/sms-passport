<?php

namespace Smartmobe\SmsPassport\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


class ApiAuthController extends AccessTokenController
{
    public function test(){
        return 123;
    }
    public function issueToken(ServerRequestInterface $request)
    {
        $rule = ['username'=>'required','password'=>'required'];

        $validator = Validator::make($request->getParsedBody(),$rule);
        if($validator->fails()){
            return $this->responseValidationError($validator->messages(),'Username and password is required');
        }
        if(!in_array($request->getServerParams()['HTTP_HOST'],config('smspassport.hosts'))){
            throw new AccessDeniedHttpException('Forbidden');
        }

        $request = $request->withParsedBody(array_merge($request->getParsedBody(),[
            'grant_type'=>'password',
            'client_id'=>config('smspassport.baseclient.'.config('app.env').'.id'),
            'client_secret'=>config('smspassport.baseclient.'.config('app.env').'.secret')
        ]));
        return parent::issueToken($request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request){
        $request->user()->token()->revoke();
        return $this->responseOk(null);
    }

    /**
     * @param ServerRequestInterface $request
     * @return Response
     */
    public function refreshToken(ServerRequestInterface $request){
        $rule = ['refresh_token'=>'required'];
        $validator = Validator::make($request->getParsedBody(),$rule);
        if($validator->fails()){
            return $this->responseValidationError($validator->messages(),'Refresh token is required');
        }
        if(!in_array($request->getServerParams()['HTTP_HOST'],config('smspassport.hosts'))){
            throw new AccessDeniedHttpException('Forbidden');
        }

        $request = $request->withParsedBody(array_merge($request->getParsedBody(),[
            'grant_type'=>'refresh_token',
            'client_id'=>config('smspassport.baseclient.'.config('app.env').'.id'),
            'client_secret'=>config('smspassport.baseclient.'.config('app.env').'.secret')
        ]));
        return parent::issueToken($request);
    }

    /**
     * @param ServerRequestInterface $request
     * @return Response
     */
    public function clientToken(ServerRequestInterface $request){
        if(!in_array($request->getServerParams()['HTTP_HOST'],config('smspassport.hosts'))){
            throw new AccessDeniedHttpException('Forbidden');
        }

        $request = $request->withParsedBody(array_merge($request->getParsedBody(),[
            'grant_type'=>'client_credentials',
            'client_id'=>config('smspassport.baseclient.'.config('app.env').'.id'),
            'client_secret'=>config('smspassport.baseclient.'.config('app.env').'.secret')
        ]))->withMethod('POST');
        return parent::issueToken($request);
    }
}
