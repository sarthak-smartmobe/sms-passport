<?php

namespace Smartmobe\SmsPassport;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;


class SmsPassportServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    protected $namespace = 'Smartmobe\SmsPassport\Controllers';
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/smspassport.php' => config_path('smspassport.php'),
        ]);
        $this->app->make('Smartmobe\SmsPassport\Controllers\ApiAuthController');
        Passport::routes();
        Passport::tokensExpireIn(Carbon::now()->addDays(1));
        Passport::enableImplicitGrant();
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(2));
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom( __DIR__.'/config/smspassport.php', 'smspassport');
        $this->map();
        $this->app->register(\Laravel\Passport\PassportServiceProvider::class);
    }
    public function map(){
        $this->mapApiRoutes();
    }
    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace' => $this->namespace,
            'prefix' => 'api',
        ], function ($router) {
            require('routes/api.php');
        });
    }
}
