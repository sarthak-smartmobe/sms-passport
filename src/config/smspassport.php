<?php
/**
 * Created by PhpStorm.
 * User: sms-suyog
 * Date: 5/17/17
 * Time: 1:11 PM
 */
return [
    "hosts" => [
        'localhost',
        'localhost:8000',
        'localhost:5555',
        'backend.xalintas.dev',
        'xalintas.dev',
        'localhost:4200',
        'localhost:8100'
    ],
    "baseclient" => [
        'local'=>[
            'id'=>env('AUTH_CLIENT_ID',1),
            'secret'=>env('AUTH_CLIENT_SECRET','PwiFzcn97KYsUHWtRyWXJKhUjeakCynRr2EyOxCE'),
        ],
        'development'=>[
            'id'=>env('AUTH_CLIENT_ID',1),
            'secret'=>env('AUTH_CLIENT_SECRET','PwiFzcn97KYsUHWtRyWXJKhUjeakCynRr2EyOxCE'),
        ],
        'production'=>[
            'id'=>env('AUTH_CLIENT_ID',1),
            'secret'=>env('AUTH_CLIENT_SECRET','PwiFzcn97KYsUHWtRyWXJKhUjeakCynRr2EyOxCE'),
        ]
    ]
];